SELECT DISTINCT film.naziv FROM film
JOIN drzava
ON drzava.iddrzava = film.drzava_porekla
JOIN film_glumac
ON film.idfilm = film_glumac.idfilm
JOIN glumac 
ON film_glumac.idglumac = glumac.idglumac
WHERE drzava_porekla = drzavljanin;
